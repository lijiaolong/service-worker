const CACHE_VERSION  = 'cache-v1';
const CACHE_FILES = [
  '/static/js/app.js',
  '/static/css/style.css',
  '/static/img/bg.jpg',
  '/'
];
self.addEventListener('install', function (event) { 
    console.log("install");
    event.waitUntil( 
        caches.open(CACHE_VERSION)//CacheStorage 
            .then(function (cache) {
                return cache.addAll(CACHE_FILES);
            }).catch(err => {
                console.log(err);
            })
    );
});
//当 Service Worker 安装成功后，便被激活
self.addEventListener('activate', function (event) {
    console.log("active");
    event.waitUntil( 
        caches.keys().then(function(keys){
            return Promise.all(keys.map(function(key, i){ 
                if(key !== CACHE_VERSION){
                    return caches.delete(keys[i]);
                }
            }))
        })
    )
});
//首次使用 Service Worker 的页面需要再次加载才会受其控制。
// 在页面发起http请求时，service worker可以通过fetch事件拦截请求，并且给出自己的响应
self.addEventListener('fetch', function (event) { 
    console.log("fetch");
    event.respondWith( // （fetchEvent接口的方法：返回页面的资源请求）
        caches.match(event.request).then(function(res){ // 判断缓存是否命中
            //console.log(event.request);
            if(res){  // 返回缓存中的资源
                return res;
            }
            requestBackend(event); // 建立新请求
        })
    )
});

function requestBackend(event){  // 请求备份操作
    var url = event.request.clone();
    return fetch(url).then(function(res){ // fetch(url, option)支持两个参数，option中可以设置header、body、method信息
        //if not a valid response send the error
        if(!res || res.status !== 200 || res.type !== 'basic'){
            return res;
        }
        var response = res.clone();
        caches.open(CACHE_VERSION).then(function(cache){ // 缓存从线上获取的资源
            cache.put(event.request, response);
        });
        return res;
    })
}
//它还不在 W3C WEB API 标准中
//在 Chrome 中这也只是一个实验性功能，需要访问 chrome://flags/#enable-experimental-web-platform-features ，开启该功能，然后重启生效。
self.addEventListener('sync', event => {
  if (event.tag === 'submit') {
    console.log('sync!');
  }
});
//ServiceWorker 运行于独立的沙盒中，无法直接访问当前页面的 DOM 等信息，但是通过 postMessage API，可以实现他们之间的消息传递。

self.addEventListener('message', function(ev) {
    console.log(ev.data);
});

//销毁，是否销毁由浏览器决定，如果一个service worker长期不使用或者机器内存有限，则可能会销毁这个worker 